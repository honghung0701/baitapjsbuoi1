//Bài 1: tính tiền lương nhân viên

//Đầu vào:
//Số ngày làm


//Đầu ra:
//Lương của nhân viên

var luong1ngay = 100000;
var soNgayLam = 20;
var luong = luong1ngay * soNgayLam;
console.log('Lương: ', luong);



//Bài 2: Tính giá trị trung bình

//Đầu vào:
//5 số thực được nhập vào


//Đầu ra:
//Giá trị trung bình của 5 số thực

var sothu1 = 12;
var sothu2 = 52;
var sothu3 = 432;
var sothu4 = 43;
var sothu5 = 485;
var trungbinh = (sothu1 + sothu2 + sothu3 + sothu4 + sothu5)/5;
console.log('Giá trị trung bình = ', trungbinh);

//Bài tập 3: Quy đổi tiền

//Đâu vào:
//Số tiền USD người dùng nhập vào

//Đầu ra: 
//Xuất ra số tiền VNĐ

var tien1USD = 23500;
var sotiencandoi = 6;
var tiendadoi = tien1USD * sotiencandoi;
console.log('Số tiền sau quy đổi = ',tiendadoi);

//Bài 4: Tính diện tích, chu vi của hình chữ nhật

//Đầu vào:
//Chiều dài và chiều rộng

//Đầu ra:
//Chu vi và diện tích hình chữ  nhật

var chieudai = 5;
var chieurong = 7;
var chuvi = (chieudai + chieurong)*2;
var dientich = chieudai * chieurong;
console.log('Chu vi = ', chuvi);
console.log('Diện tích = ', dientich);

//Bài 5: Tính tổng 2 ký số

//Đầu vào:
//Sô có 2 chữ sô được nhập vào

//Đầu ra:
//Tổng của 2 ký số vừa nhập̀̉̉̉̉

var so = 65;
var tong = (so % 10) + (Math.floor(so/10));
console.log('Số = ', so);
console.log('Tổng 2 ký số = ', tong);
